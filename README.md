# aneurysm_dl

## Requirements
Install python requirements using pip, preferably in a virtual environment, via
``$ pip3 install -r requirements.txt``

Note:
If you get an error using the ``dataset`` module, concerning rtree or libspatialindex, make sure the latter is installed
before installing rtree.
On Ubuntu you can simply run
``$ sudo apt install libspatialindex-dev``
before installing rtree.

## Usage
### Pre-Processing
#### Converting DICOM and mesh data to Numpy
Convert a DICOM dataset to a Numpy array without altering the image data. Additionally, create a binary Numpy mask from
a corresponding 3D mesh.  
```python ./dataset/dataset.py --convert_to_np DCM_DIR.dcm MESH_DIR.obj OUT_DIR(optional)```

#### Create Subvolumes
Scale images that are too large and optionally perform data augmentation.  
```python ./preprocessing/create_subvolumes.py YAML_DIR OUT_DIR DIM N_AUGS SETS```  

For example:  
```python ./preprocessing/create_subvolumes.py ./data/training_data.yaml ./somedir/ 256 10 training,validation,test```

#### Create Projections
Create and save nine different projections from previously created subvolumes and optionally augment them.  
```python ./preprocessing/create_projections.py YAML_DIR OUT_DIR DIM N_AUGS SETS```  

### Training and Inference
For a list of all parameters etc. call  
```python ./main.py -h```

Example for training a network:  
```python ./main.py train ./data/training_data.yaml ./somedir/model.h5 -l ./somedir/model.csv -nc ./data/network_config.yaml -s 256 256 1 0 -n unet_2d -ie 1```

Example for inference:  
```python ./main.py infer ./data/training_data.yaml ./somedir/model.h5 -s 256 256 1 0 -pl 2d -d validation -t 0.8```

### Post-Processing
#### Threshold Search
Look for the best threshold to maximize a specified criterion for the given data.  
```python ./postprocessing/threshold_search.py YAML_DIR MODEL_DIR SLICES ROWS COLUMNS METRICS THRESHOLD_RANGE DATASETS```

For example:  
```python ./lpostprocessing/threshold_search.py ./data/training_data.yaml ./somedir/model.h5 0 256 256 dice,1 0.1,0.9,0.1 validation```

