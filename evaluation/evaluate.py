#!/usr/bin/env python
import sys
sys.path.insert(0, './aneurysm_dl/evaluation/')
sys.path.insert(0, '.\\evaluation\\')
import numpy as np
from metrics import sensitivity, specificity, dice, precision


def evaluate(x_true, x_est):
    # x_true: ground truth segmentation, x_est: binary segmentation output
    d = dice(x_true, x_est)
    sens = sensitivity(x_true, x_est)
    spec = specificity(x_true, x_est)
    prec = precision(x_true, x_est)
    print('dice: ', d)
    print('sens: ', sens)
    print('spec: ', spec)
    print('prec: ', prec)
    return {'dice': d, 'sensitivity': sens, 'specificity': spec, 'precision': prec}


def fmap_to_binary(fmap, threshold=None):
    out = np.zeros(fmap.shape)
    # convert feature map (output of CNN) to binary segmentation
    if threshold is not None:
        out[np.where(fmap > threshold)] = 1
    return out


if __name__ == '__main__':
    pass
