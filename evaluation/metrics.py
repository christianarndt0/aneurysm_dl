#!/usr/bin/env python
import numpy as np


def dice(im1, im2):
    # calculate binary Dice coefficient for 2 boolean numpy arrays
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    # compute Dice coefficient
    intersection = np.logical_and(im1, im2)
    return 2. * intersection.sum() / (im1.sum() + im2.sum())


def confusion(x, y):
    # calculate the confusion matrix of segmentation and ground truth
    x = np.asarray(x).astype(np.bool)  # truth
    y = np.asarray(y).astype(np.bool)  # estimate

    if x.shape != y.shape:
        raise ValueError("Shape mismatch: x and y must have the same shape.")

    tp = np.logical_and(x, y)
    tn = np.logical_and(np.logical_not(x), np.logical_not(y))

    fp = np.logical_and(np.logical_not(x), y)
    fn = np.logical_and(np.logical_not(y), x)

    return tp, tn, fp, fn


def specificity(x, y):
    # calulate specificity / TNR
    tp, tn, fp, fn = confusion(x, y)
    tn, fp = float(tn.sum()), float(fp.sum())
    try:
        return tn / (tn + fp)
    except ZeroDivisionError:
        print('WARNING: Division by zero.')
        return None


def sensitivity(x, y):
    # calulate sensitivity / TPR / recall
    tp, tn, fp, fn = confusion(x, y)
    tp, fn = float(tp.sum()), float(fn.sum())
    try:
        return tp / (tp + fn)
    except ZeroDivisionError:
        print('WARNING: Division by zero.')
        return None


def precision(x, y):
    # calculate precision / positive predictive value
    tp, tn, fp, fn = confusion(x, y)
    tp, fp = float(tp.sum()), float(fp.sum())
    try:
        return tp / (tp + fp)
    except ZeroDivisionError:
        print('WARNING: Division by zero.')
        return None


if __name__ == '__main__':
    estimate = np.array([0, 0, 1, 1, 1, 0])
    truth = np.array([0, 0, 0, 1, 0, 0])
    print(confusion(truth, estimate))
    print(precision(truth, estimate))
