"""
# Usage with passing figure and numpy array
fig, ax = plt.subplots(1, 1)

X = np.load('..\\data\\test\\v0000.npy')

tracker = IndexTracker(ax, X.reshape(X.shape[:3]))
fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
plt.show()
"""

from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
import sys
from skimage import io


class SliceViewer(object):
    def __init__(self, X, ax=None, fig=None, order='rcsc'):
        """
        View numpy volume slice by slice
        :param X: numpy array: shape (X, Y, Z, 1) or (X, Y, Z, 3)
        :param ax:
        :param fig:
        :param order: String, default='rcsc': slice, row, column, channel ('srcc') or row, column, slice, channel ('rcsc')
        """
        self.order = order
        if ax is not None and fig is not None:
            self.fig = fig
            self.ax = ax
        else:
            self.fig, self.ax = plt.subplots(1, 1)
        self.ax.set_title('use scroll wheel to navigate images')

        if isinstance(X, str):
            self.X = np.load(X)
            # self.X = self.X.reshape(self.X.shape[:3])
        else:
            self.X = X
            # self.X = self.X.reshape(self.X.shape[:3])
        if self.order == 'rcsc':
            rows, cols, self.slices, self.channels = self.X.shape
        elif self.order == 'srcc':
            self.slices, rows, cols, self.channels = self.X.shape
        if self.channels == 1:
            self.X = self.X.reshape(self.X.shape[:3])
        self.ind = self.slices//2

        if self.order == 'rcsc':
            self.im = self.ax.imshow(self.X[:, :, self.ind])
        elif self.order == 'srcc':
            self.im = self.ax.imshow(self.X[self.ind, :, :])
        self.update()

    def onscroll(self, event):
        # print("%s %s" % (event.button, event.step))
        if event.button == 'up':
            self.ind = (self.ind + 1) % self.slices
        else:
            self.ind = (self.ind - 1) % self.slices
        self.update()

    def update(self):
        if self.order == 'rcsc':
            self.im.set_data(self.X[:, :, self.ind])
        elif self.order == 'srcc':
            self.im.set_data(self.X[self.ind, :, :])
        self.ax.set_ylabel('slice %s' % self.ind)
        self.im.axes.figure.canvas.draw()

    def show(self):
        self.fig.canvas.mpl_connect('scroll_event', self.onscroll)
        plt.show()


if __name__ == '__main__':
    im = sys.argv[1]
    if im[-3:] == 'tif':
        im = io.imread(im)
        im = np.reshape(im, (im.shape[0], im.shape[1], im.shape[2], 1))
    sv = SliceViewer(im, order='srcc')
    sv.show()
