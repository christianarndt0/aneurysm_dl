try:
    from slice_viewer import SliceViewer
except ImportError:
    from slice_viewer.slice_viewer import SliceViewer
import numpy as np
import sys


class SliceViewerMulti:
    def __init__(self, volumes, mode='montage', order='rcsc', normalize=True):
        """
        Init SliceViewer for multiple volumes
        :param volumes [str/numpy array]: list of volumes (or directories of volumes) to visualize
        :param mode: str, default='montage': 'montage' for next to each other, 'stack' for semi-transparent overlay
        :param order: str, default='rcsc': Dimensions of data. Row, column, slice, channel (rcsc) ir slice, row, column, channel (srcc)
        :param normalize: bool, default=True: Normalize all input volumes to a range of 0...1
        """
        # TODO 'single' mode: plot new figure for each volume in separate thread  # easier: button press to switch between?
        self.mode = mode
        for i, arg in enumerate(volumes):
            if isinstance(arg, str):
                v = np.load(arg)
                # v = v.reshape(v.shape[:3])
            else:
                v = arg
                # v = v.reshape(v.shape[:3])
            # normalize volume
            v = v.astype(np.float32)
            v = v / np.max(v[:]) if normalize else v
            # create viewer depending on type
            if self.mode == 'montage':
                if i == 0:
                    volume = v
                else:
                    volume = np.concatenate((volume, v), axis=1) if order == 'rcsc' else np.concatenate((volume, v), axis=2)
            if self.mode == 'stack':
                v = v.reshape(v.shape[:3])
                if i == 0:
                    volume = np.stack((v, v, v), axis=-1)
                else:
                    v = np.stack((v, v, v), axis=-1)
                    if i == 1:
                        channel = 0
                    elif i == 2:
                        channel = 1
                    elif i == 3:
                        channel = 2
                    # TODO add more channels or just error
                    volume[:, :, :, channel] = volume[:, :, :, channel] + v[:, :, :, channel] * 0.5
        self.viewer = SliceViewer(volume, order=order)

    def show(self):
        self.viewer.show()


if __name__ == '__main__':
    # TODO better parsing
    svm = SliceViewerMulti(sys.argv[3:], mode=sys.argv[1], order=sys.argv[2])
    # svm = SliceViewerMulti(['..\\..\\datasets\\Hannover_LR\\npy\\Hannover_LR.npy', '..\\..\\datasets\\Hannover_LR\\npy\\Hannover_LRmask.npy', '..\\..\\datasets\\Hannover_LR\\npy\\Hannover_LR2mask.npy'], mode='montage', order='srcc')
    svm.show()
