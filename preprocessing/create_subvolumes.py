#!/usr/bin/env python
from __future__ import print_function
import sys
sys.path.insert(0, './aneurysm_dl/')
sys.path.insert(0, '..\\aneurysm_dl')
import os
import yaml
import numpy as np
from scipy.ndimage import zoom, measurements
import dataset.augment as aug
import dataset.numpy_utils as npu
from slice_viewer.slice_viewer_multi import SliceViewerMulti
from skimage import exposure


def scale(v, res):
    # v: 3d volume to be scaled, res: target resolution of shape[1] i.e. rows (srcc order)
    if v.shape[1] == res:
        return v
    # scale volume
    f = float(res) / v.shape[1]
    v2 = zoom(v, (f, f, f, 1)).astype(v.dtype)
    if v2.shape[1] != res or v2.shape[2] != res:
        raise ArithmeticError('Rounding caused dimension mismatch.')
    return v2


def to_hu(v, intercept, slope, clip=(None, None)):
    if clip[0] is not None or clip[1] is not None:
        return np.clip(v * slope + intercept, clip[0], clip[1])
    else:
        return v * slope + intercept


def get_voi(v, mask, dim):
    s, r, c = measurements.center_of_mass(mask)[:3]
    return npu.get_subvolume(v, (s, r, c), dim), npu.get_subvolume(mask, (s, r, c), dim)


if __name__ == '__main__':
    # usage: python create_subvolumes.py YAML_DIR OUT_DIR DIM N_AUGS SETS
    # e.g. python reate_subvolumes.py ./data.yaml ./somedir/ 256 10 training,validation,test
    try:
        yaml_dir, out_dir, dim, n_augs, sets = sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]), sys.argv[5]
    except IndexError as e:
        print('Usage: $ python create_subvolumes.py YAML_DIR OUT_DIR DIM N_AUGS SETS')
        raise IndexError(e)
    # don't augment, just rescale, if number of augmentations == 1
    if n_augs == 0:
        n_augs = 1
        probs = [0, 0, 0, 0, 0]
    else:
        try:
            probs = [float(sys.argv[6]), float(sys.argv[7]), float(sys.argv[8]), float(sys.argv[9]), float(sys.argv[10])]
        except IndexError:
            probs = [0.3, 0.3, 0.3, 0.3, 0.3]

    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)

    new_info = {'test': {}, 'training': {}, 'validation': {}}
    # determine which datasets to augment
    sets = sets.split(',')
    # calculate total number of resulting datasets
    i, total = 0, 0
    for s in sets:
        total += len(info[s]) * n_augs
    # create subvolumes
    for s in sets:
        pths = []
        for d in info[s]:
            # load image and mask
            im = np.load(d['image'])
            mask = np.load(d['mask'])
            # transform to HU if number of augmentations == 1, i.e. when only rescaling from originals
            # im = to_hu(im, d['intercept'], d['slope'], clip=(None, None)) if n_augs == 1 else im
            # scale if necessary
            # im = scale(im, dim)
            # mask = scale(mask, dim)
            for n in range(n_augs):
                # perform data augmentation
                im2, mask2 = aug.augment3d(im, mask=mask, flip=False, probabilities=probs)
                im2, mask2 = get_voi(im2, mask2, (min(im.shape[0], 256), 256, 256))
                im2 = exposure.rescale_intensity(im2, in_range=(np.median(im[:]), 0.95*np.max(im[:])), out_range=(0, 255))
                im2, mask2 = np.round(im2).astype(np.uint8), mask2.astype(np.uint8)
                # save data
                index = str(i)
                while len(index) < len(str(total)):
                    index = '0' + index
                impath = os.path.abspath(os.path.join(out_dir, 'v{}_{}.npy'.format(dim, index)))
                np.save(impath, im2)
                maskpath = os.path.abspath(os.path.join(out_dir, 'v{}_{}_mask.npy'.format(dim, index)))
                np.save(maskpath, mask2)
                # save paths
                pths.append({'image': impath, 'mask': maskpath})
                # increment counter and print progress
                i += 1
                print('{} / {}\n'.format(i, total))
                # show images (for debug)
                print(np.max(im2[:]), np.min(im2[:]), np.mean(im2[:]), np.std(im2[:]))
                svm = SliceViewerMulti([im2, mask2], order='srcc', mode='montage', normalize=False)
                svm.show()
        # update yaml
        new_info[s] = pths
    # save yaml
    with open(os.path.join(out_dir, 'data{}.yaml'.format(dim)), 'w') as file:
        yaml.dump(new_info, file, default_style=None, default_flow_style=False)
