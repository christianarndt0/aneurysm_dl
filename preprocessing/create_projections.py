#!/usr/bin/env python
from __future__ import print_function
import sys
sys.path.insert(0, './aneurysm_dl/')
sys.path.insert(0, './aneurysm_dl/dataset/')
import os
import yaml
import numpy as np
from skimage.transform import resize
from skimage import exposure
import matplotlib.pyplot as plt
from scipy.ndimage.interpolation import rotate
import operator
import augment as aug


def get_orth_mip(im, mask, axis):
    im = np.max(im, axis=axis)
    mask = np.max(mask, axis=axis)
    return im, mask


def get_orth_avg(im, mask, axis):
    im = np.sum(im, axis=axis) / im.shape[axis]
    mask = np.sum(mask, axis=axis) / mask.shape[axis]
    return im, mask


def get_orth_std(im, mask, axis):
    im = np.std(im, axis=axis)
    mask = np.std(mask, axis=axis)
    return im, mask


def get_mip(im, mask, view):
    if view == 0 or view == 1 or view == 2:
        im = np.max(im, axis=view)
        mask = np.max(mask, axis=view)
    elif view == 3 or view == 4:
        im = rotate(im, 45, axes=(1, 2))
        mask = rotate(mask, 45, axes=(1, 2))
        im = np.max(im, axis=1) if view == 3 else np.max(im, axis=2)
        mask = np.max(mask, axis=1) if view == 3 else np.max(mask, axis=2)
    elif view == 5 or view == 6:
        im = rotate(im, 45, axes=(0, 1))
        mask = rotate(mask, 45, axes=(0, 1))
        im = np.max(im, axis=0) if view == 5 else np.max(im, axis=1)
        mask = np.max(mask, axis=0) if view == 5 else np.max(mask, axis=1)
    elif view == 7 or view == 8:
        im = rotate(im, 45, axes=(0, 2))
        mask = rotate(mask, 45, axes=(0, 2))
        im = np.max(im, axis=0) if view == 7 else np.max(im, axis=2)
        mask = np.max(mask, axis=0) if view == 7 else np.max(mask, axis=2)
    return im, mask


def project_mask(im, mask, view):
    if view == 0:
        v = np.repeat(mask[np.newaxis, :, :, :], im.shape[0], 0)
    elif view == 1:
        v = np.repeat(mask[:, np.newaxis, :, :], im.shape[1], 1)
    elif view == 2:
        v = np.repeat(mask[:, :, np.newaxis, :], im.shape[2], 2)
    elif view == 3:
        s = np.sqrt(im.shape[1]**2 + im.shape[2]**2).astype(np.int)
        v = np.repeat(mask[:, np.newaxis, :, :], s, 1)
        v = rotate(v, -45, axes=(1, 2))
    elif view == 4:
        s = np.sqrt(im.shape[1]**2 + im.shape[2]**2).astype(np.int)
        v = np.repeat(mask[:, :, np.newaxis, :], s, 2)
        v = rotate(v, -45, axes=(1, 2))
    elif view == 5:
        s = np.sqrt(im.shape[0]**2 + im.shape[1]**2).astype(np.int)
        v = np.repeat(mask[np.newaxis, :, :, :], s, 0)
        v = rotate(v, -45, axes=(0, 1))
    elif view == 6:
        s = np.sqrt(im.shape[0]**2 + im.shape[1]**2).astype(np.int)
        v = np.repeat(mask[:, np.newaxis, :, :], s, 1)
        v = rotate(v, -45, axes=(0, 1))
    elif view == 7:
        s = np.sqrt(im.shape[0]**2 + im.shape[2]**2).astype(np.int)
        v = np.repeat(mask[np.newaxis, :, :, :], s, 0)
        v = rotate(v, -45, axes=(0, 2))
    elif view == 8:
        s = np.sqrt(im.shape[0]**2 + im.shape[2]**2).astype(np.int)
        v = np.repeat(mask[:, :, np.newaxis, :], s, 2)
        v = rotate(v, -45, axes=(0, 2))
    v = crop_center(v, im.shape)
    return v.astype(im.dtype)


def crop_center(img, bounding):
    start = tuple(map(lambda a, da: a//2-da//2, img.shape, bounding))
    end = tuple(map(operator.add, start, bounding))
    slices = tuple(map(slice, start, end))
    return img[slices]


if __name__ == '__main__':
    # usage: python create_projections.py YAML_DIR OUT_DIR DIM N_AUGS SETS
    # e.g. python create_projections.py ./data.yaml ./somedir/ 256 10 training,validation,test
    try:
        yaml_dir, out_dir, dim, n_augs, sets = sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]), sys.argv[5]
    except IndexError as e:
        print('Usage: $ python create_projections.py YAML_DIR OUT_DIR DIM N_AUGS SETS')
        raise IndexError(e)
    # don't augment, just rescale, if number of augmentations == 1
    if n_augs == 0:
        n_augs = 1
        probs = [0, 0, 0, 0, 0]
    else:
        try:
            probs = [float(sys.argv[6]), float(sys.argv[7]), float(sys.argv[8]), float(sys.argv[9]), float(sys.argv[10])]
        except IndexError:
            probs = [0.3, 0.3, 0.3, 0.3, 0.3]
    try:
        start = int(sys.argv[11])
    except IndexError:
        start = 0
    DEBUG = True if out_dir == 'DEBUG' else False

    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)

    new_info = {'test': {}, 'training': {}, 'validation': {}}
    # determine which datasets to augment
    sets = sets.split(',')
    # calculate total number of resulting datasets
    i, total = 0, 0
    for s in sets:
        total += len(info[s]) * n_augs * 9
    # create projections
    for s in sets:
        pths = []
        for nset, d in enumerate(info[s]):
            if nset < start:
                i += 9 * n_augs
                continue
            # load image and mask
            im3d = np.load(d['image'])
            mask3d = np.load(d['mask'])
            test = np.zeros_like(im3d)
            for n in range(9):
                # create projections
                _mip, _mask = get_mip(im3d, mask3d, n)
                # scale
                if _mip.shape != (dim, dim, 1):
                    _mip = resize(_mip.astype(np.float32), (dim, dim, 1), anti_aliasing=True)
                    _mask = resize(_mask.astype(np.float32), (dim, dim, 1), anti_aliasing=True)
                # cast data types
                _mip, _mask = _mip.astype(im3d.dtype), _mask.astype(mask3d.dtype)
                # augment data
                for a in range(n_augs):
                    mip, mask = aug.augment2d(_mip, _mask, probabilities=probs)
                    # mip = exposure.rescale_intensity(mip, in_range=(np.median(mip[:]), 0.95 * np.max(mip[:])), out_range=(0, 255))
                    # show images (for debug)
                    if DEBUG:
                        mask2 = mask * np.max(mip[:])
                        mip2 = mip * np.clip(mask2, 0.7, 1.)
                        vis = np.concatenate((mip2.reshape(mip2.shape[0], mip2.shape[1]), mask2.reshape(mask2.shape[0], mask2.shape[1])), 1)
                        plt.imshow(vis)
                        plt.show()

                        v = project_mask(im3d, mask, n)
                        test += v
                        vis = np.max(test, axis=0)
                        plt.imshow(vis.reshape((vis.shape[0], vis.shape[1])))
                        plt.show()
                    else:
                        # save data
                        index = str(i)
                        while len(index) < len(str(total)):
                            index = '0' + index
                        impath = os.path.abspath(os.path.join(out_dir, 'p{}.npy'.format(index)))
                        np.save(impath, mip)
                        maskpath = os.path.abspath(os.path.join(out_dir, 'p{}_mask.npy'.format(index)))
                        np.save(maskpath, mask)
                        # save paths
                        pths.append({'image': impath, 'mask': maskpath})
                    # increment counter and print progress
                    i += 1
                    print('{} / {}\n'.format(i, total))

        # update yaml
        new_info[s] = pths
    # save yaml
    if not DEBUG:
        with open(os.path.join(out_dir, os.path.split(yaml_dir)[-1][:-5] + '_slices.yaml'), 'w') as file:
            yaml.dump(new_info, file, default_style=None, default_flow_style=False)
