from __future__ import print_function
import numpy as np
import sys
import yaml


if __name__ == '__main__':
    # print class imbalance and mean / std over the whole dataset
    yaml_dir = sys.argv[1]

    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)

    ones = 0.
    mean = 0.
    for i, d in enumerate(info['training']):
        # count number of class 1 in mask
        mask = np.load(d['mask'])
        ones = ones + np.sum(mask)
        # calculate mean gray value
        im = np.load(d['image']).flatten()
        mean = mean + np.mean(im)
    mean = mean / (i+1)
    ones = ones / (i+1)
    print('Mean number of ones: {}'.format(ones))

    # calculate standard deviation
    sos = 0.
    pixels = 0.
    for i, d in enumerate(info['training']):
        # calculate sum of square differences
        im = np.load(d['image']).flatten()
        pixels = pixels + im.shape[0]
        sos = sos + np.sum((im - mean)**2)
    std = np.sqrt((1 / (pixels-1)) * sos)
    print('Mean: {}, Std: {}'.format(mean, std))
