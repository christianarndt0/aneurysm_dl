import sys
sys.path.insert(0, './aneurysm_dl/dataset/')
sys.path.insert(0, './aneurysm_dl/models/')
sys.path.insert(0, '.\\models\\')
sys.path.insert(0, './aneurysm_dl/evaluation/')
sys.path.insert(0, '.\\evaluate\\')
import numpy as np
import yaml
import keras
from infer import feed_forward, feed_forward_3d
import metrics
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


def median(lst):
    n = len(lst)
    s = sorted(lst)
    return (sum(s[n//2-1:n//2+1])/2.0, s[n//2])[n % 2] if n else None


def get_score(y, yhat, scores):
    # compute (weighted) score
    d = 0
    for n, s in enumerate(scores):
        if s == 'dice':
            d = d + metrics.dice(y, yhat) * float(scores[n+1])
        elif s == 'spec':
            d = d + metrics.specificity(y, yhat) * float(scores[n+1])
        elif s == 'sens':
            d = d + metrics.sensitivity(y, yhat) * float(scores[n+1])
        elif s == 'prec':
            try:
                d = d + metrics.precision(y, yhat) * float(scores[n+1])
            except TypeError:
                d = d
    return d


if __name__ == '__main__':
    try:
        yaml_dir, model_dir, slices, rows, columns, scores, trange, dset = sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), sys.argv[6], sys.argv[7], sys.argv[8]
    except IndexError as e:
        print('Usage: python threshold_search.py YAML_DIR MODEL_DIR SLICES ROWS COLUMNS METRICS THRESHOLD_RANGE DATASETS')
        raise IndexError(e)

    # parse metrics: metric1,weight1,metric2,weight2,...
    scores = scores.split(',')

    # parse threshold range: from,to,step
    trange = trange.split(',')

    # load validation data paths
    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)

    # load trained model
    model = keras.models.load_model(model_dir, compile=False)

    dim = (slices, rows, columns, 1) if slices > 0 else (rows, columns, 1)
    threshold = 0.5
    best_score = 0
    scrs = []
    for t in np.arange(float(trange[0]), float(trange[1]), float(trange[2])):
        d = 0
        # perform inference
        for i in range(len(info[dset])):
            if slices > 0:
                x, y, xhat, yhat = feed_forward_3d(model, info[dset][i], dim, threshold=t)
            else:
                x, y, xhat, yhat = feed_forward(model, info[dset][i], dim, threshold=t)
            # compute (weighted) score
            score = get_score(y, yhat, scores)
            d = d + score
            scrs.append(score)
        # save best threshold
        if d > best_score:
            best_score = d
            threshold = t
    # print best threshold
    print('Best threshold: {}, with average score of: {} and median: {}\n\r'.format(threshold, best_score/len(info[dset]), median(scrs)))
