import numpy as np
from keras.models import Model
from keras.layers import Input
from keras.layers import Conv3D, MaxPooling3D, UpSampling3D, concatenate


def create(input_shape, pool_size=(2, 2, 2), depth=4, n_base_filters=32, n_labels=1):
    """
    Build a keras unet3d of the 3D Unet proposed by Cicec et al. in 2016
    :param input_shape: ( n_channels (int), x_size(int), y_size(int), z_size(int) ): Shape of the input image(s).
    The x, y and z sizes must be divisible by pool_size^depth.
    :param pool_size: ( x_size(int), y_size(int), z_size(int) ): Pooling dimensions applied after every step
    :param depth: int=4: number of downward (and upward) steps in the Unet.
    :param n_base_filters: int=32: Number of filters in the first step. Doubles every step.
    :param n_labels: int=1: Number of different labels in the segmentation
    :return:
    """
    # input layer
    inputs = Input(input_shape)
    # save output layer of each step for concatenation in upsampling path
    layers = []
    # feature extraction path
    current_layer = inputs
    for d in range(depth):
        conv1 = Conv3D(n_base_filters*(2**d), (3, 3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(current_layer)
        conv2 = Conv3D(n_base_filters*(2**d), (3, 3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(conv1)
        layers.append(conv2)
        if d < depth-1:
            current_layer = MaxPooling3D(pool_size=pool_size, padding='same')(conv2)
        else:
            current_layer = conv2
    # upsampling path
    for d in range(depth-2, -1, -1):
        upconv = UpSampling3D(pool_size, data_format='channels_last')(current_layer)
        merge = concatenate([layers[d], upconv])
        conv1 = Conv3D(n_base_filters*(2**d), (3, 3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(merge)
        current_layer = Conv3D(n_base_filters*(2**d), (3, 3, 3), padding='same', activation='relu', kernel_initializer='he_normal')(conv1)
    # output layer
    outputs = Conv3D(n_labels, (1, 1, 1), padding='same', activation='sigmoid', kernel_initializer='he_normal')(current_layer)

    model = Model(inputs=inputs, outputs=outputs)
    print(model.summary())
    return model



