import sys
import os
sys.path.insert(0, './aneurysm_dl/dataset/')
sys.path.insert(0, './aneurysm_dl/evaluation')
sys.path.insert(0, './aneurysm_dl/preprocessing')
import keras
import numpy as np
try:
    from slice_viewer.slice_viewer_multi import SliceViewerMulti
except ImportError as e:
    print('Slice Viewer import failed. -> {}'.format(e))
import yaml
try:
    from dataset import numpy_utils as npu
except ImportError:
    import numpy_utils as npu
try:
    from evaluation.evaluate import fmap_to_binary, evaluate
except ImportError:
    from evaluate import fmap_to_binary, evaluate
try:
    from preprocessing import create_projections as cp
except ImportError:
    import create_projections as cp
import csv
import time
import math
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.transform import resize


def pad_volume(v, dim):
    # pad image volume to get equally spaced tiles
    slices, rows, columns = v.shape[:3]
    # number of tiles in each axis
    ns, nr, nc = int(np.ceil(float(slices) / dim[0])), int(np.ceil(float(rows) / dim[1])), int(np.ceil(float(columns) / dim[2]))
    # create empty image
    impad = np.zeros((ns*dim[0], nr*dim[1], nc*dim[2], dim[3]))
    # insert original values
    sc, rc, cc = impad.shape[0] / 2, impad.shape[1] / 2, impad.shape[2] / 2  # image center
    impad[int(sc-slices/2):int(sc+slices/2), int(rc-rows/2):int(rc+rows/2), int(cc-columns/2):int(cc+columns/2), :] = v[:, :, :, :]
    return impad, [ns, nr, nc]  # padded volume, number of tiles for each dimension


def remove_padding(v, dim):
    # v: volume, dim: dimension of the unpadded image
    # remove padding (take center part of image according to dim (padding is done by keeping image in the center))
    sc, rc, cc = v.shape[0] / 2, v.shape[1] / 2, v.shape[2] / 2  # image center
    out = v[int(sc-dim[0]/2):int(sc+dim[0]/2), int(rc-dim[1]/2):int(rc+dim[1]/2), int(cc-dim[2]/2):int(cc+dim[2]/2), :]
    return out


def get_tiles(volume, ntiles, dim):
    # TODO add overlap
    tiles = []
    for s in range(ntiles[0]):
        for r in range(ntiles[1]):
            for c in range(ntiles[2]):
                tiles.append(volume[s*dim[0]:(s+1)*dim[0], r*dim[1]:(r+1)*dim[1], c*dim[2]:(c+1)*dim[2], :])
    # return list of tiles and number of tiles in each direction, iteration order: columns, rows, slices
    return tiles


def merge_tiles(fmaps, centers, dim):
    merged = np.empty(dim)
    weights = np.zeros(dim)
    for f, c in zip(fmaps, centers):
        merged[c[0]-int(f.shape[0]/2):c[0]+int(f.shape[0]/2), c[1]-int(f.shape[1]/2):c[1]+int(f.shape[1]/2), c[2]-int(f.shape[2]/2):c[2]+int(f.shape[2]/2), :] += f
        weights[c[0]-int(f.shape[0]/2):c[0]+int(f.shape[0]/2), c[1]-int(f.shape[1]/2):c[1]+int(f.shape[1]/2), c[2]-int(f.shape[2]/2):c[2]+int(f.shape[2]/2), :] += 1
    return merged / weights


def get_tile_centers(imshape, modelshape):
    # least number of necessary tiles for each dimension
    ns = math.ceil(float(imshape[0]) / modelshape[0])
    nr = math.ceil(float(imshape[1]) / modelshape[1])
    nc = math.ceil(float(imshape[2]) / modelshape[2])

    # get all coordinate combinations for each tile
    coords = []
    for s in np.linspace(modelshape[0]/2, imshape[0]-modelshape[0]/2, ns):
        for r in np.linspace(modelshape[1]/2, imshape[1]-modelshape[1]/2, nr):
            for c in np.linspace(modelshape[2]/2, imshape[2]-modelshape[2]/2, nc):
                coords.append((s, r, c))

    return coords


def save_results_as_csv(results, outdir, i=0):
    with open(outdir, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        head = []
        row = []
        for k in results.keys():
            head.append(k)
            row.append(results[k])
        if i == 0:
            writer.writerow(head)
        writer.writerow(row)


def feed_forward_proj(model, data, dim2d=(256, 256, 1), dim3d=None, threshold=0.5):
    # load image and mask
    im, mask = npu.get_data(data, dim3d, flip=False, add_batch=False, standardize=True)
    out = np.zeros_like(im)
    # create projections and perform inference on each one
    for i in range(9):
        _mip, _mask = cp.get_mip(im, mask, i)
        # scale
        if _mip.shape != dim2d:
            _mip = resize(_mip.astype(np.float32), dim2d, anti_aliasing=True)
            _mask = resize(_mask.astype(np.float32), dim2d, anti_aliasing=True)
        # add batch dimension
        shape = list(dim2d)
        shape.insert(0, 1)
        mip = np.empty(shape).astype(_mip.dtype)
        mip[0,] = _mip
        # perform inference
        pred = model.predict(mip, batch_size=1)
        # project feature map back into 3d volume
        v = cp.project_mask(im, pred[0], i)
        out += v
    out = out / 9
    # threshold feature map 
    pred_bin = fmap_to_binary(out, threshold=threshold)
    return im, mask, out, pred_bin
 # returns without batch dimension


def feed_forward_3d(model, data, dim, threshold=0.5, src=None):
    # load image and mask
    im, mask = npu.get_data(data, dim, flip=False, add_batch=True, standardize=True, src=src)
    # perform inference
    pred = model.predict(im, batch_size=1)
    # threshold feature map to get binary segmentation
    pred_bin = fmap_to_binary(pred, threshold=threshold)
    return im[0], mask[0], pred[0], pred_bin[0]


def feed_forward(model, data, dim, threshold=0.5):
    # load image and mask
    im, mask = npu.get_data(data, dim, flip=False, add_batch=True, standardize=True)
    # perform inference
    pred = model.predict(im, batch_size=1)
    # threshold feature map to get binary segmentation
    pred_bin = fmap_to_binary(pred, threshold=threshold)
    return im[0], mask[0], pred[0], pred_bin[0]


def infer(yaml_dir, model_dir, dim, threshold=0.5, threshold2=0.5, visualize=False, indices=None, mode='2d', dataset='test', out_dir=None):
    # parse multiple model directories (put 2d network dir first for mode = 'both')
    model_dir = model_dir.split(',')
    # load data to classify
    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)
    # load model
    model = keras.models.load_model(model_dir[0], compile=False)
    # perform inference
    for i, data in enumerate(info[dataset]):
        # only perform inference on certain datasets
        if indices is not None and i not in indices:
            continue
        if mode == '2d':
            # feed forward 2d
            im, mask, pred, pred_bin = feed_forward(model, data, dim, threshold=threshold)
        elif mode == '3d':
            # perform 3d inference
            im, mask, pred, pred_bin = feed_forward_3d(model, data, dim, threshold=threshold)
        elif mode == 'proj':
            # get prediction from different mip views
            im, mask, pred, pred_bin = feed_forward_proj(model, data, threshold=threshold)
        elif mode == 'both':
            # get prediction from different mip views
            im, mask, pred, pred_bin = feed_forward_proj(model, data, threshold=threshold)
            # get center of mass - quick and preliminary -> TODO use criterion that makes more sense
            thres = np.max(pred[:])
            count = 0;
            iters = 0;
            # include voxels until "standard" aneurysm size is reached
            while (count < 20000):
                _pred = pred > thres
                count = np.sum(_pred[:])
                thres = thres * 0.98
                iters = iters + 1
                if iters > 100:
                    break
            print(iters)
            
            center = ndimage.measurements.center_of_mass(pred * _pred)[:3]
            print('center of mass: ({}, {}, {})'.format(center[0], center[1], center[2])) 

            # perform 3d inference around center of mass
            model = keras.models.load_model(model_dir[1], compile=False)
            im2, mask2, pred2, pred_bin2 = feed_forward_3d(model, data, dim, threshold=threshold2, src=center)
            # combine results
            array = np.zeros_like(im)
            pred2 = npu.put_subvolume(array, center, pred2)
            pred_bin2 = npu.put_subvolume(array, center, pred_bin2)
        else:
            print('Mode \'{}\' unknown. Use \'2d\', 3d\', \'proj\' or \'both\'.'.format(mode))
            break
        # calculate evaluation metrics
        metrics = evaluate(mask, pred_bin2) if mode == 'both' else evaluate(mask, pred_bin)
        # show results
        # if visualize:
            # plt.imshow(pred[0, :, :, 0])
            # plt.show()
        # save data
        if out_dir is not None:
            # save images
            if visualize:
                np.save(os.path.join(out_dir, 'im_{}.npy'.format(i)), im)
                np.save(os.path.join(out_dir, 'mask_{}.npy'.format(i)), mask)
                np.save(os.path.join(out_dir, 'pred_{}.npy'.format(i)), pred)
                np.save(os.path.join(out_dir, 'pred_bin_{}.npy'.format(i)), pred_bin)
                if mode == 'both':
                    np.save(os.path.join(out_dir, 'pred2_{}.npy'.format(i)), pred2)
                    np.save(os.path.join(out_dir, 'pred_bin2_{}.npy'.format(i)), pred_bin2)
            # TODO save evaluation data to csv
            save_results_as_csv(metrics, os.path.join(out_dir, 'metrics.csv'), i=i)


if __name__ == '__main__':
    yaml_dir = '..\\..\\datasets256\\training_data256_windows.yaml'
    model_dir = '..\\..\\trained_models\\test.h5'
    do_vis = True
    indices = [0, 1, 2, 3]  # which dataset(s) to test from test set defined in training_data.yaml

    # load pre-trained model
    model = keras.models.load_model(model_dir, compile=False)
    # load data to classify
    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)

    for i in indices:
        # inference on a single test volume
        x, y, xhat, yhat = feed_forward(model, info['test'][i], dim=(64, 64, 64, 1), threshold=-0.1, do_tiling=False)

        # visualize output
        if do_vis:
            volumes = [x, y, xhat, yhat] if y is not None else [x, xhat, yhat]
            svm = SliceViewerMulti(volumes, order='srcc', normalize=False)
            svm.show()

        # compute evaluation metrics
        if y is not None:
            evaluate(y, yhat)
