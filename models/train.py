from models.unet3d import cicek2016, myunet3d
from models.unet2d import myunet2d
from dataset.data_generator import DataGenerator
import keras
import yaml
import csv
from keras import backend as K
import tensorflow as tf
import time


def focal_loss(gamma=2., alpha=.25, eps=1e-5):
    def focal_loss_fixed(y_true, y_pred):
        y_pred = K.clip(y_pred, eps, 1.-eps)  # improve numerical stability
        pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
        pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
        if gamma > 0.01:
            return -alpha * K.mean(K.pow(1. - pt_1, gamma) * K.log(pt_1)) - (1.-alpha) * K.mean(K.pow(pt_0, gamma) * K.log(1. - pt_0))
        else:
            return -alpha * K.mean((1. - pt_1) * K.log(pt_1)) - (1.-alpha) * K.mean(pt_0 * K.log(1. - pt_0))
    return focal_loss_fixed


def jaccard_distance_loss(smooth=1):
    def jaccard_distance_smoothed(y_true, y_pred):
        intersection = K.sum(K.abs(y_true * y_pred), axis=-1)
        sum_ = K.sum(K.abs(y_true) + K.abs(y_pred), axis=-1)
        jac = (intersection + smooth) / (sum_ - intersection + smooth)
        return (1 - jac) * smooth
    return jaccard_distance_smoothed


def dice_loss(smooth=1, eps=1e-12):
    def dice_coef(y_true, y_pred):
        y_pred = K.clip(y_pred, eps, 1.-eps)  # improve numerical stability
        y_true_f = K.flatten(y_true)
        y_pred_f = K.flatten(y_pred)
        intersection = K.sum(y_true_f * y_pred_f)
        dice = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
        return 1. - dice
    return dice_coef


def cos_dice_loss(q=2, smooth=1, eps=1e-12):
    def cos_dice(y_true, y_pred):
        y_pred = K.clip(y_pred, eps, 1.-eps)  # improve numerical stability
        y_true_f = K.flatten(y_true)
        y_pred_f = K.flatten(y_pred)
        intersection = K.sum(y_true_f * y_pred_f)
        dice = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
        # return -tf.math.log(dice)
        return K.pow(tf.math.cos(1.5708*dice), q)
    return cos_dice


def save_history_as_csv(history, outdir):
    with open(outdir, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        # head line containing column names
        columns = ['timestamp', 'epoch']
        for h in history:
            columns.append(h)
        writer.writerow(columns)
        # write data row by row
        for i in range(len(history['loss'])):
            row = [time.time(), i+1]
            for c in columns:
                if c == 'timestamp' or c == 'epoch':
                    continue
                row.append(history[c][i])
            writer.writerow(row)


def get_optimizer(params):
    if params['type'] == 'sgd':
        opt = keras.optimizers.SGD(lr=params['learning_rate'], momentum=params['momentum'], decay=params['decay'], nesterov=bool(params['nesterov']), clipnorm=params['clipnorm'])
    elif params['type'] == 'adadelta':
        opt = keras.optimizers.Adadelta(clipnorm=params['clipnorm'])
    elif params['type'] == 'adam':
        opt = keras.optimizers.Adam(lr=params['learning_rate'], clipnorm=params['clipnorm'])
    else:
        raise ValueError('No optimizer named {} available'.format(params['type']))
    return opt


def get_loss(params):
    if params['type'] == 'focal':
        loss = [focal_loss(alpha=params['alpha'], gamma=params['gamma'], eps=params['epsilon'])]
    elif params['type'] == 'jaccard':
        loss = [jaccard_distance_loss(smooth=params['smooth'])]
    elif params['type'] == 'dice':
        loss = [dice_loss(smooth=params['smooth'], eps=params['epsilon'])]
    elif params['type'] == 'cosdice':
        loss = [cos_dice_loss(q=params['q'], smooth=params['smooth'])]
    else:
        loss = params['type']
    return loss


def lr_scheduler(decay=0.1, epochs=[]):
    def scheduler(epoch, lr):
        if epoch in epochs:
            return lr * decay
        return lr
    return scheduler


def train(yaml_dir, model_dir, network_config, dim, history_dir='', initial_epoch=1):
    # create data generators for training and validation
    with open(yaml_dir, 'r') as file:
        info = yaml.load(file)
    train_gen = DataGenerator(info['training'], 1, dim, sigma=network_config['data_generator']['sigma'], mean=network_config['data_generator']['mean'], std=network_config['data_generator']['std'])
    val_gen = DataGenerator(info['validation'], 1, dim, sigma=network_config['data_generator']['sigma'], mean=network_config['data_generator']['mean'], std=network_config['data_generator']['std'])

    # create model
    c = network_config['network_params']
    if initial_epoch > 1:
        # load existing model
        if network_config['loss']['type'] == 'focal':
             model = keras.models.load_model(model_dir, custom_objects={'focal_loss_fixed': get_loss(network_config['loss'])[0]})
        elif network_config['loss']['type'] == 'dice':
             model = keras.models.load_model(model_dir, custom_objects={'dice_coef': get_loss(network_config['loss'])[0]})
        elif network_config['loss']['type'] == 'cosdice':
             model = keras.models.load_model(model_dir, custom_objects={'cos_dice': get_loss(network_config['loss'])[0]})
    elif network_config['architecture'] == 'cicek':
        model = cicek2016.create(dim, pool_size=c['pool_size'], depth=c['depth'], n_base_filters=c['n_base_filters'])
    elif network_config['architecture'] == 'myunet3d':
        model = myunet3d.create(dim, pool_size=c['pool_size'], depth=c['depth'], n_base_filters=c['n_base_filters'], which=c['which'])
    elif network_config['architecture'] == 'myunet2d':
        model = myunet2d.create(dim, pool_size=c['pool_size'], depth=c['depth'], n_base_filters=c['n_base_filters'], which=c['which'])
    else:
        raise ValueError('No architecture named {} available.'.format(network_config['architecture']))

    # learning scheduler
    callbacks = []
    try:
        callbacks.append(keras.callbacks.LearningRateScheduler(lr_scheduler(decay=network_config['scheduler']['decay'], epochs=network_config['scheduler']['epochs']), verbose=1))
    except KeyError:
        pass

    # save model checkpoints regularly
    callbacks.append(keras.callbacks.ModelCheckpoint(model_dir, verbose=1, save_best_only=True, save_weights_only=False))
    if history_dir:
        callbacks.append(keras.callbacks.CSVLogger(history_dir, append=True))

    # stop early if validation loss doesn't improve
    callbacks.append(keras.callbacks.EarlyStopping(patience=network_config['patience']))

    # terminate when invalid loss is encountered
    callbacks.append(keras.callbacks.TerminateOnNaN())

    # compile model
    if initial_epoch == 1:
        model.compile(optimizer=get_optimizer(network_config['optimizer']), loss=get_loss(network_config['loss']))

    # train model
    history = model.fit_generator(generator=train_gen, validation_data=val_gen, use_multiprocessing=False, epochs=network_config['epochs'], callbacks=callbacks, initial_epoch=initial_epoch-1)

    # save model
    # model.save(model_dir)


if __name__ == '__main__':
    with open('..\\data\\network_config.yaml', 'r') as file:
                conf = yaml.load(file)
    train('..\\data\\training_data_windows.yaml', '..\\data\\test\\test_model.h5', conf['unet_3d_baseline'], (64, 64, 64, 1))
