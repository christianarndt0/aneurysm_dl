import numpy as np
from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, concatenate, Dropout


def create(input_shape, pool_size=(2, 2), depth=4, n_base_filters=32, n_labels=1, which=0):
    # parse params
    kernel_initializer = 'he_normal'

    # NETWORK 0
    if which == 0:
        # input layer
        inputs = Input(input_shape)
        # save output layer of each step for concatenation in upsampling path
        layers = []
        # feature extraction path
        current_layer = inputs
        for d in range(depth):
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', activation='relu', kernel_initializer=kernel_initializer)(current_layer)
            conv2 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', activation='relu', kernel_initializer=kernel_initializer)(conv1)
            layers.append(conv2)
            if d < depth-1:
                current_layer = MaxPooling2D(pool_size=pool_size, padding='same')(conv2)
            else:
                current_layer = conv2
        # upsampling path
        for d in range(depth-2, -1, -1):
            upconv = UpSampling2D(pool_size, data_format='channels_last')(current_layer)
            merge = concatenate([layers[d], upconv])
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', activation='relu', kernel_initializer=kernel_initializer)(merge)
            current_layer = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', activation='relu', kernel_initializer=kernel_initializer)(conv1)
        # output layer
        #current_layer = Conv2D(2, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_initializer)(current_layer)
        outputs = Conv2D(n_labels, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_initializer)(current_layer)

    # NETWORK1
    if which == 1:
        # input layer
        inputs = Input(input_shape)
        # save output layer of each step for concatenation in upsampling path
        layers = []
        # feature extraction path
        current_layer = inputs
        for d in range(depth):
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(current_layer)
            conv2 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(conv1)
            layers.append(conv2)
            if d < depth-1:
                current_layer = MaxPooling2D(pool_size=pool_size, padding='same')(conv2)
                #current_layer = Dropout(0.5)(current_layer)
            else:
                #drop1 = Dropout(0.5)(conv2)
                current_layer = current_layer  #drop1
        # upsampling path
        for d in range(depth-2, -1, -1):
            upconv = UpSampling2D(pool_size, data_format='channels_last')(current_layer)
            merge = concatenate([layers[d], upconv])
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(merge)
            conv2 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(conv1)
            current_layer = conv2
        # output layer
        outputs = Conv2D(n_labels, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_initializer)(current_layer)

    # NETWORK2
    if which == 2:
        # input layer
        inputs = Input(input_shape)
        # save output layer of each step for concatenation in upsampling path
        layers = []
        # feature extraction path
        current_layer = inputs
        for d in range(depth):
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(current_layer)
            conv2 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(5, 5), activation='relu', kernel_initializer=kernel_initializer)(conv1)
            layers.append(conv2)
            if d < depth-1:
                current_layer = MaxPooling2D(pool_size=pool_size, padding='same')(conv2)
            else:
                drop1 = Dropout(1.0)(conv2)
                current_layer = drop1
        # upsampling path
        for d in range(depth-2, -1, -1):
            upconv = UpSampling2D(pool_size, data_format='channels_last')(current_layer)
            merge = concatenate([layers[d], upconv])
            conv1 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(1, 1), activation='relu', kernel_initializer=kernel_initializer)(merge)
            conv2 = Conv2D(n_base_filters*(2**d), (3, 3), padding='same', dilation_rate=(5, 5), activation='relu', kernel_initializer=kernel_initializer)(conv1)
            current_layer = conv2
        # output layer
        outputs = Conv2D(n_labels, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_initializer)(current_layer)

    # return model
    model = Model(inputs=inputs, outputs=outputs)
    print(model.summary())
    return model

