from __future__ import print_function
import os
import argparse
from models.train import train
try:
    from models.infer import infer, _infer
except ImportError as e:
    print(e)
    print('Inference will not work in this environment')
import yaml
import sys
import keras.backend as K

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


class Parser:
    def __init__(self, debug=False):
        # init parser
        self.parser = argparse.ArgumentParser(description='Train CNN or perform inference')
        # add arguments
        self.parser.add_argument('mode', default='infer', choices=['infer', 'train'], nargs='?', help="[Training/Inference] Choose between training a network or perform inference on an existing one.")
        self.parser.add_argument('yaml_dir', help="[Training/Inference]Path to the YAML file describing the dataset.")
        self.parser.add_argument('model_dir', help="[Training/Inference] Path to the .h5 model to use for inference or where to save trained model after training.")
        self.parser.add_argument('--indices', nargs='+', type=int, default=[], help='[Inference] Choose which segmentation results to save to disk.')
        self.parser.add_argument('--network', '-n', type=str, default='unet_3d_baseline', help='[Training] Network architecture.')
        self.parser.add_argument('--network_config', '-nc', type=str, default='data/network_config.yaml', help='[Training] Path to the network_config.yaml describing architecture and training parameters.')
        self.parser.add_argument('--size', '-s', nargs=4, type=int, default=[64, 64, 64, 1], help='[Training/Inference] Image volume size. Order: srcc [slices, rows, columns, channels]')
        self.parser.add_argument('--threshold', '-t', type=float, default=0.5, help='[Inference] Threshold to apply to feature map to extract binary mask.')  
        self.parser.add_argument('--threshold2', '-t2', type=float, default=0.5, help='[Inference] Threshold to apply to feature map to extract binary mask. Only used for 3d inference with --pipeline=both') 
        self.parser.add_argument('--visualize', '-v', action='store_true', help='[Inference] Visualize inference output.')
        self.parser.add_argument('--logging', '-l', type=str, default='', help='[Training/Inference] Save training/inference statistics as csv under this directory, e.g. /myfolder/myfile.history')
        self.parser.add_argument('--mean', '-m', type=float, default=None, help='[Inference] Mean value to subtract during normalization')
        self.parser.add_argument('--std', type=float, default=None, help='[Inference] Standard deviation used as divisor during normalization')
        self.parser.add_argument('--initial_epoch', '-ie', type=int, default=1, help='[Training] Initial epoch from which to start training.')
        self.parser.add_argument('--dataset', '-d', type=str, default='test', help='[Inference] Dateset name (training, validation or test) to perform inference on')
        self.parser.add_argument('--floatx', '-fx', type=int, default=32, help='[Training] Floating point precision to be used for training.')
        self.parser.add_argument('--pipeline', '-pl', type=str, default='2d', help='[Inference] 2d, 3d or both.')
        # query arguments
        if debug:
            self.args = vars(self.parser.parse_args('infer data\\training_data.yaml data\\test\\test_model.h5 -s 64 64 64 1 -v'.split()))
        else:
            self.args = vars(self.parser.parse_args())
        # postprocess arguments
        try:
            if self.args['size'][3] == 0:
                self.args['size'] = self.args['size'][:3]
        except KeyError:
            pass
        # set parameters
        if self.args['floatx'] == 16:
            K.set_floatx('float16')
            K.set_epsilon(1e-4)

    def run(self):
        # print parameters
        print('Using the following parameters:\n')
        for a in self.args.keys():
            print(a, ':', self.args[a])
        # train model or perform inference
        if self.args['mode'] == '_infer':
            _infer(self.args['yaml_dir'], self.args['model_dir'], self.args['size'], self.args['threshold'],
                  csv_path=self.args['logging'], visualize=self.args['visualize'], indices=self.args['indices'], mean=self.args['mean'], std=self.args['std'],
                  dataset=self.args['dataset'])
        elif self.args['mode'] == 'infer':
            infer(self.args['yaml_dir'], self.args['model_dir'], self.args['size'], threshold=self.args['threshold'], threshold2=self.args['threshold2'], visualize=self.args['visualize'], indices=self.args['indices'], mode=self.args['pipeline'], dataset=self.args['dataset'], out_dir=self.args['logging'])

        elif self.args['mode'] == 'train':
            # get network config
            with open(self.args['network_config'], 'r') as file:
                conf = yaml.load(file)
            # choose network architecture
            try:
                net = conf[self.args['network']]
            except KeyError:
                raise KeyError('No network named {} defined in YAML file.'.format(self.args['network']))
            train(self.args['yaml_dir'], self.args['model_dir'], net, tuple(self.args['size']), history_dir=self.args['logging'], initial_epoch=self.args['initial_epoch'])


if __name__ == '__main__':
    debug = False if len(sys.argv) > 1 else True
    # init parser
    parser = Parser(debug=debug)
    print(os.getcwd())
    # run program
    parser.run()
