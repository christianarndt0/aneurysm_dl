#!/usr/bin/env python
import numpy as np
from imgaug import augmenters as iaa
#from slice_viewer.slice_viewer_multi import SliceViewerMulti
try:
    from numpy_utils import normalize, get_subvolume
except ImportError:
    from dataset.numpy_utils import normalize, get_subvolume
from scipy import ndimage
import random


def augment_axis(v, aug, axis=None):
    # random axis if none is given
    a = random.randint(1, 3) if axis is None else axis
    if a == 1:  # rotate around slice axis
        v = np.moveaxis(v, 0, -1)
        v = aug.augment_image(v)
        v = np.moveaxis(v, -1, 0)
    elif a == 2:  # rotate around row axis
        v = np.moveaxis(v, 1, -1)
        v = aug.augment_image(v)
        v = np.moveaxis(v, -1, 1)
    elif a == 3:  # rotate around column axis
        v = aug.augment_image(v)
    return v


def augment3d(v, mask=None, flip=True, probabilities=(0.5, 0.5, 0.5, 0.5)):
    # probabilities for augmentations to happen in the following order: rotation, shearing, contrast, blur
    s = v.shape
    v = v[:, :, :, 0]
    mask = mask[:, :, :, 0] if mask is not None else None

    # flip
    if flip:
        ax = random.randint(-1, 2)
        if ax >= 0:
            v = np.flip(v, ax)
            mask = np.flip(mask, ax) if mask is not None else None

    # rotation
    if random.random() < probabilities[0]:
        ang = random.randint(-30, 30)
        ax = random.randint(1, 3)
        aug = iaa.Affine(rotate=ang)
        v = augment_axis(v, aug, axis=ax)
        mask = augment_axis(mask, aug, axis=ax) if mask is not None else None

    # shearing
    if random.random() < probabilities[1]:
        ang = random.randint(-15, 15)
        ax = random.randint(1, 3)
        aug = iaa.Affine(shear=ang)
        v = augment_axis(v, aug, axis=ax)
        mask = augment_axis(mask, aug, axis=ax) if mask is not None else None

    # contrast
    if random.random() < probabilities[2]:
        r = random.uniform(0.9, 1.1)
        aug = iaa.GammaContrast(gamma=r)
        v = aug.augment_image(v)

    # blur
    if random.random() < probabilities[3]:
        r = random.choice([3, 5])
        aug = iaa.MedianBlur(k=r)
        v = aug.augment_image(v)

    # gaussian noise
    if random.random() < probabilities[4]:
        r = random.uniform(0.01, 0.04)
        aug = iaa.AdditiveGaussianNoise(scale=r * np.max(v[:]), per_channel=True)
        v = aug.augment_image(v)

    vout = np.empty(s)
    vout[:, :, :, 0] = v[:, :, :]
    if mask is not None:
        mout = np.empty(s)
        mout[:, :, :, 0] = mask[:, :, :]
    else:
        mout = None
    return vout, mout


def augment2d(im, mask, probabilities=[0, 0, 0, 0, 0]):
    # rotation
    if random.random() < probabilities[0]:
        ang = random.randint(-30, 30)
        aug = iaa.Affine(rotate=ang)
        im = aug.augment_image(im)
        mask = aug.augment_image(mask)

    # shearing
    if random.random() < probabilities[1]:
        ang = random.randint(-15, 15)
        aug = iaa.Affine(shear=ang)
        im = aug.augment_image(im)
        mask = aug.augment_image(mask)

    # contrast
    if random.random() < probabilities[2]:
        r = random.uniform(0.9, 1.1)
        aug = iaa.GammaContrast(gamma=r)
        im = aug.augment_image(im)

    # blur
    if random.random() < probabilities[3]:
        r = random.choice([3, 5])
        aug = iaa.MedianBlur(k=r)
        im = aug.augment_image(im)

    # gaussian noise
    if random.random() < probabilities[4]:
        r = random.uniform(0.01, 0.04)
        aug = iaa.AdditiveGaussianNoise(scale=r * np.max(im[:]), per_channel=True)
        im = aug.augment_image(im)

    return im, mask


if __name__ == '__main__':
    # TODO load and augment all training and validation images
    im = np.load('C:\\Users\\chrar\\Documents\\MSE\\Project\\datasets256\\Hannover_AB\\Hannover_AB.npy').astype(np.uint16)
    mask = np.load('C:\\Users\\chrar\\Documents\\MSE\\Project\\datasets256\\Hannover_AB\\Hannover_AB_right_closedAneurysmHead_merged.npy')

    im2, mask2 = augment3d(im, mask=mask, probabilities=[1, 1, 1, 1, 1], flip=False)

    # get subvolumes
    s, r, c = ndimage.measurements.center_of_mass(mask)[:3]
    im = get_subvolume(im, (s, r, c), (128, 128, 128, 1))
    mask = get_subvolume(mask, (s, r, c), (128, 128, 128, 1))
    s, r, c = ndimage.measurements.center_of_mass(mask2)[:3]
    im2 = get_subvolume(im2, (s, r, c), (128, 128, 128, 1))
    mask2 = get_subvolume(mask2, (s, r, c), (128, 128, 128, 1))

    im = normalize(im, zero_to_one=False)
    im2 = normalize(im2, zero_to_one=False)

    # show image
    #svm = SliceViewerMulti([im, mask, im2, mask2], order='srcc', normalize=False)
    #svm.show()
