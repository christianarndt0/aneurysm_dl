#!/usr/bin/env python3
from __future__ import print_function
import pydicom
import trimesh
from trimesh import proximity
import numpy as np
import csv
import sys
import os
import copy
import yaml
from scipy import ndimage
try:
    from dataset.numpy_utils import merge_binary_masks
except ImportError:
    from numpy_utils import merge_binary_masks
import ast
from PIL import Image
from skimage import io


class Dataset:
    def __init__(self):
        self.dcm = None
        self.pixel_array = None
        self.mesh = None
        self.mask = None

        self.dcm_path = None
        self.mesh_path = None

        # conversion matrix
        self.m = np.zeros((4, 4))

    def load(self, dcm, mesh):
        # import dicom file
        self.dcm = pydicom.dcmread(dcm, force=False)
        print('Modality: {}'.format(self.dcm.Modality))
        # if no image data is available in DICOM file, check if .tif image with the same name exists in the same folder
        try:
            s = self.dcm.pixel_array.shape
            self.pixel_array = self.dcm.pixel_array
        except AttributeError:
            tif = dcm[:-3] + 'tif'
            self.pixel_array = io.imread(tif)
        # check if it is only a single slice
        if len(self.pixel_array.shape) == 2:
            # TODO load all slices from directory and concatenate to 3D volume
            files = os.listdir(os.path.split(dcm)[0])
            pixel_array = np.empty((len(files), self.pixel_array.shape[0], self.pixel_array.shape[1])).astype(self.pixel_array.dtype)
            for i, f in enumerate(files):
                slice = pydicom.dcmread(os.path.join(os.path.split(dcm)[0], f), force=False)
                pixel_array[i, :, :] = slice.pixel_array
            self.pixel_array = pixel_array
        print('Slices x Rows x Columns: {} x {} x {}'.format(self.pixel_array.shape[0], self.pixel_array.shape[1], self.pixel_array.shape[2]))
        self.dcm_path = dcm
        # compute conversion matrix
        self.m = self.compute_m()
        # import 3D segmentation mesh
        self.mesh = trimesh.load(mesh)
        self.mesh_path = mesh

    def compute_m(self):
        f12, f22, f32, f11, f21, f31 = self.dcm.ImageOrientationPatient  # (0020, 0037)
        sx, sy, sz = self.dcm.ImagePositionPatient  # (0020, 0032)
        dr, dc = self.dcm.PixelSpacing  # (0028, 0030)
        ds = self.dcm.SliceThickness  # (0018, 0050)
        n = np.cross(np.array([f12, f22, f32]), np.array([f11, f21, f31]))

        return np.array([[f11*dr, f12*dc, n[0]*ds, sx],
                         [f21*dr, f22*dc, n[1]*ds, sy],
                         [f31*dr, f32*dc, n[2]*ds, sz],
                         [0, 0, 0, 1]])

    def get_pixel_coordinates(self, row, column, slice):
        v = np.array([row, column, slice, 1])
        points = self.m.dot(v)
        return points[0], points[1], points[2]

    def generate_mask(self, make_csv=False, zstep=1):
        # limit search to ROI (= bounding box of the 3D mesh)
        bounds = self.mesh.bounds
        # lower and upper bounds in image coordinates
        l = np.linalg.inv(self.m).dot(np.array([bounds[0, 0], bounds[0, 1], bounds[0, 2], 1]))
        l = np.floor(l[:3] / l[3])
        u = np.linalg.inv(self.m).dot(np.array([bounds[1, 0], bounds[1, 1], bounds[1, 2], 1]))
        u = np.ceil(u[:3] / u[3])
        # indices to search in
        rows = np.arange(l[0], u[0]).astype(np.uint16)
        columns = np.arange(l[1], u[1]).astype(np.uint16)
        slices = np.arange(l[2], u[2], step=zstep).astype(np.uint16)

        # copy original dcm file and set pixel array to zeros to hold store the mask
        self.mask = np.zeros(self.pixel_array.shape)

        # loop objects and variables
        pq = proximity.ProximityQuery(self.mesh)
        i = 0
        imax = len(rows) * len(columns) * len(slices)
        # find all voxel within 3d mesh
        for s in slices:
            for c in columns:
                for r in rows:
                    # print progress
                    i += 1
                    print('{} / {} ({}%)'.format(i, imax, round((i*100) / imax), 2), end='\r')
                    # points inside have positive distance
                    x, y, z = self.get_pixel_coordinates(r, c, s)
                    if pq.signed_distance(np.array([x, y, z]).reshape(1, 3)) > 0:
                        self.mask[s, r, c] = 1
                        # append point coordinates to csv
                        if make_csv:
                            with open(self.mesh_path[:-4] + '_mask.xyz', 'a') as csvfile:
                                writer = csv.writer(csvfile, delimiter=' ')
                                writer.writerow([str(x), str(y), str(z)])
        # user information
        print('{} / {} ({}%)'.format(i, imax, round((i*100) / imax), 2))
        print('Done.')

    def save_dcm_as_np(self, outdir=None, filename=None, type='passthrough'):
        """
        Save image data and mask as numpy arrays
        :param outdir: String, default=None: output directory of npy files. Defaults to DCM and mesh directories
        :param filename: String, default=None: base name of npy files. Mask file automatically gets 'mask' appended to the name  # TODO not working with merging
        :param type: String, default='passthrough': Save data as it is loaded ('passthrough') or scale to 0..1 float32 ('normalized')
                                                    Note: Mask is always Int8
        """
        # get path and filename of dcm file and mesh file
        image_dir, image_name = os.path.split(self.dcm_path)
        mask_dir, mask_name = os.path.split(self.mesh_path)
        # if no directory or filename is given, save data according to dcm and obj file
        if outdir is not None:
            image_dir = outdir
            mask_dir = outdir
        if filename is None:
            image_name = os.path.splitext(image_name)[0]
            mask_name = os.path.splitext(mask_name)[0]
        else:
            image_name = filename
            mask_name = filename + '_mask'
        # save data
        if type == 'passthrough':
            image = self.pixel_array
        elif type == 'normalized':
            image = self.pixel_array.astype(np.float32)
            image = image / image.max()
        image = np.reshape(image, (image.shape[0], image.shape[1], image.shape[2], 1))

        mask = np.clip(self.mask, 0, 1).astype(np.int8)
        mask = np.reshape(mask, (mask.shape[0], mask.shape[1], mask.shape[2], 1))
        np.save(os.path.join(image_dir, image_name), image)
        np.save(os.path.join(mask_dir, mask_name), mask)

        # write info file
        self.write_dataset_info(image_dir, image_name, mask_dir, mask_name)

    def write_dataset_info(self, image_dir, image_name, mask_dir, mask_name):
        # calc center of mass of mask
        center = ndimage.measurements.center_of_mass(self.mask)

        info_name = os.path.splitext(image_name)[0] + '.yaml'
        if os.path.isfile(os.path.join(image_dir, info_name)):
            # load existing info
            with open(os.path.join(image_dir, info_name), 'r') as file:
                info = yaml.load(file)
            # merge masks
            print('Merging masks')
            merged = merge_binary_masks(np.load(info['mask']), np.load(os.path.join(mask_dir, mask_name + '.npy')))
            np.save(info['mask'][:-4] + '_merged', merged)
            # update yaml
            with open(os.path.join(image_dir, info_name), 'w') as file:
                info['mask'] = info['mask'][:-4] + '_merged.npy'
                info['aneurysms'] = ast.literal_eval(info['aneurysms'])
                info['aneurysms'].append(center)
                info['aneurysms'] = str(info['aneurysms'])
                yaml.dump(info, file, default_style=None, default_flow_style=False)
        else:
            info = dict(
                image=os.path.abspath(os.path.join(image_dir, image_name + '.npy')),
                mask=os.path.abspath(os.path.join(mask_dir, mask_name + '.npy')),
                aneurysms=str([center])  # slice, row, column
            )
            with open(os.path.join(image_dir, info_name), 'w') as file:
                yaml.dump(info, file, default_style=None, default_flow_style=False)


if __name__ == '__main__':
    # usage: python dataset.py --convert_to_np DCM_DIR.dcm MESH_DIR.obj OUT_DIR(optional)
    if sys.argv[1] == '--convert_to_np':
        d = Dataset()
        d.load(sys.argv[2], sys.argv[3])
        d.generate_mask(make_csv=False, zstep=1)
        try:
            outdir = sys.argv[4]
        except IndexError:
            outdir = None
        d.save_dcm_as_np(outdir=outdir)
