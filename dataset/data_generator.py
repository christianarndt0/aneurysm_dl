import sys
sys.path.insert(0, './aneurysm_dl/dataset/')
sys.path.insert(0, '.\\dataset\\')
import numpy as np
import keras
from scipy.ndimage import zoom
from scipy import ndimage
import random
try:
    import dataset.numpy_utils as npu
except ImportError:
    import numpy_utils as npu
try:
    import dataset.augment as aug
except ImportError:
    import augment as aug


class DataGenerator(keras.utils.Sequence):
    def __init__(self, datasets, batch_size, dim, shuffle=True, sigma=0.05, mean=None, std=None):
        self.datasets = datasets
        self.batch_size = batch_size
        self.dim = dim
        self.shuffle = shuffle
        self.indexes = None
        self.on_epoch_end()

        self.sigma = sigma * dim[0]
        self.mean = mean
        self.std = std

    def on_epoch_end(self):
        """ Update indexes after each epoch. """
        self.indexes = np.arange(len(self.datasets))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, IDs):
        """ Generate data containing batch_size samples. """
        # initialization
        shape = list(self.dim)
        shape.insert(0, 1)
        X = np.empty(shape).astype(np.float32)
        y = np.empty(shape).astype(np.uint8)
        # generate data
        for i, id in enumerate(IDs):
            # load data from file and apply preprocessing / augmentation
            X[i,], y[i,] = npu.get_data(id, self.dim, sigma=self.sigma, mean=self.mean, std=self.std, standardize=True)
        return X, y

    def __len__(self):
        """ Denotes the number of batches per epoch. """
        return int(np.floor(len(self.datasets) / self.batch_size))

    def __getitem__(self, index):
        """ Generate one batch of data. """
        # generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # find list of IDs
        temp_IDs = [self.datasets[i] for i in indexes]
        # generate data
        X, y = self.__data_generation(temp_IDs)
        return X, y
