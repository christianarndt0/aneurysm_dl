import numpy as np
import sys
import os
from scipy import ndimage
import random
from skimage import exposure


def flip(arr, axis):
    return np.flip(arr, axis)


def get_data(id, dim, flip=True, stretch=False, sigma=0, mean=None, std=None, src=None, standardize=False, add_batch=False):
    im = np.load(id['image'])
    mask = np.load(id['mask'])
    if dim is None:
        dim = im.shape
    # get center of mass of segmentation mask or take given values
    if src:
        s, r, c = src
        s2, r2, c2 = ndimage.measurements.center_of_mass(mask)[:3]
        print('COM deviation: {}, {}, {}'.format(s-s2, r-r2, c-c2))
    else:
        s, r, c = ndimage.measurements.center_of_mass(mask)[:3]
    # draw coordinates from normal distribution and threshold at +-2*sigma
    if sigma > 0:
        _s, _r, _c = np.random.normal(s, sigma), np.random.normal(r, sigma), np.random.normal(c, sigma)
        _s = max(_s, s-2*sigma) if _s < s else min(_s, s+2*sigma)
        _r = max(_r, r-2*sigma) if _r < r else min(_r, r+2*sigma)
        _c = max(_c, c-2*sigma) if _c < c else min(_c, c+2*sigma)
    else:
        _s, _r, _c = s, r, c
    # histogram stretching
    if stretch:
        pl, pu = np.percentile(im, (0.01, 99.99))
        im = exposure.rescale_intensity(im, in_range=(pl, pu))
    # get subvolume of image and mask
    if len(dim) == 4:
        im = get_subvolume(im, (_s, _r, _c), dim)
        mask = get_subvolume(mask, (_s, _r, _c), dim)
    # flip
    if flip:
        ax = random.randint(-1, len(dim)-2)
        if ax >= 0:
            im = np.flip(im, ax)
            mask = np.flip(mask, ax)
    # normalize volumes
    if standardize:
        im = np.array(im, dtype=np.float32) / 255.
        # im = normalize(im, mean=mean, std=std, zero_to_one=False), mask
    # add batch number
    if add_batch:
        shape = list(dim)
        shape.insert(0, 1)
        _im = np.empty(shape).astype(im.dtype)
        _mask = np.empty(shape).astype(mask.dtype)
        _im[0,] = im
        _mask[0,] = mask
        return _im, _mask
    return im, mask


def normalize(array, mean=None, std=None, zero_threshold=1e-5, zero_to_one=False):
    # normalize array so its values are between 0 and 1)
    # returns grey image (0.5 everywhere) when blank image is given
    m = np.max(array[:])
    if m < zero_threshold:
        return (np.ones(array.shape) * 0.5).astype(array.dtype)
    # normalize
    mean = np.mean(array[:]) if mean is None else mean
    std = np.std(array[:]) if std is None else std
    array = (array - mean) / std
    if zero_to_one:
        array = array - np.min(array[:])
        array = array / np.max(array[:])
    return array


def get_indices(array, center, target_dim):
    # determine bounds of original array according to center and target_dim
    s0, s1 = center[0]-target_dim[0]/2, center[0]+target_dim[0]/2
    r0, r1 = center[1]-target_dim[1]/2, center[1]+target_dim[1]/2
    c0, c1 = center[2]-target_dim[2]/2, center[2]+target_dim[2]/2

    s0, s1 = int(max(s0, 0)), int(min(s1, array.shape[0]))
    r0, r1 = int(max(r0, 0)), int(min(r1, array.shape[1]))
    c0, c1 = int(max(c0, 0)), int(min(c1, array.shape[2]))
    new_array_shape = (s1-s0, r1-r0, c1-c0)

    # move bounds so no padding is required
    if new_array_shape[0] != target_dim[0]:
        if s0 == 0:
            s1 = target_dim[0]
        elif s1 == array.shape[0]:
            s0 = array.shape[0] - target_dim[0]
    if new_array_shape[1] != target_dim[1]:
        if r0 == 0:
            r1 = target_dim[1]
        elif r1 == array.shape[1]:
            r0 = array.shape[1] - target_dim[1]
    if new_array_shape[2] != target_dim[2]:
        if c0 == 0:
            c1 = target_dim[2]
        elif c1 == array.shape[2]:
            c0 = array.shape[2] - target_dim[2]

    return s0, s1, r0, r1, c0, c1


def get_subvolume(array, center, target_dim):
    """
    Get sub-volume of a 3-dimensional numpy array around a specified point.
    :param array: numpy_array: Original volume with shape (slices, rows, columns, channels)
    :param center: Coordinates in original array around which to extract the sub-volume (slice, row, column)
    :param target_dim: Target shape of numpy array that will be returned (slices, rows, columns, channels)
    :return: numpy array around center with size target_dim
    """
    s0, s1, r0, r1, c0, c1 = get_indices(array, center, target_dim)
    return array[s0:s1, r0:r1, c0:c1, :]


def put_subvolume(array, center, subvolume):
    s0, s1, r0, r1, c0, c1 = get_indices(array, center, subvolume.shape)
    array[s0:s1, r0:r1, c0:c1, :] = subvolume
    return array


def merge_binary_masks(mask1, mask2):
    if not mask1.shape == mask2.shape:
        print('Input shapes don\'t match, returning None.')
        return None
    return np.clip(mask1 + mask2, 0, 1)


def scale_volume(volume, args):
    """
    Scale an ndarray to a target size or by a factor depending on the datatype of the arguments. Float -> scale by
    factor; Int -> scale to target dimension
    :param volume: numpy.ndarray: Array to scale
    :param args: (Int/Float, ...): Target size (for each dimension) of the image or scaling factors
    :return: ndarray: Scaled numpy array
    """
    s = volume.shape
    if type(args[0]) is int:
        nargs = []
        for i in range(len(args)):
            nargs.append(float(args[i]) / float(s[i]))
        return ndimage.zoom(volume, tuple(nargs))
    return ndimage.zoom(volume, args)


if __name__ == '__main__':
    if sys.argv[1] == 'scale':
        args = []
        for a in sys.argv[3:]:
            if '.' in a:
                args.append(float(a))
            else:
                args.append(int(a))
        volume = np.load(sys.argv[2])
        outdir, name = os.path.split(sys.argv[2])
        name = os.path.splitext(name)[0]
        np.save(os.path.join(outdir, name + '_scaled'), scale_volume(volume, tuple(args)))

    if sys.argv[1] == 'merge':
        m1 = np.load(sys.argv[2])
        m2 = np.load(sys.argv[3])
        outdir, name = os.path.split(sys.argv[2])
        name = os.path.splitext(name)[0]
        np.save(os.path.join(outdir, name + '_merged'), merge_binary_masks(m1, m2))
